#!/bin/bash

. ./functions/read.sh
. ./functions/make.sh

flush() {
    local PAYLOAD_RM=$(ls -d /tmp/*/ | grep "payload_")
    local STAGE_RM=$(ls -d /tmp/*/ | grep "stage_")
    local SCRIPT_STARTUP_RM=$(ls /tmp/script_installer*)

    if [[ ${PAYLOAD_RM} != "" ]] ; then
        rm -R $(echo ${PAYLOAD_RM})
    fi;

    if [[ ${STAGE_RM} != "" ]] ; then
        rm -R $(echo ${STAGE_RM})
    fi;

    if [[ ${SCRIPT_STARTUP_RM} != "" ]] ; then
        rm $(echo ${SCRIPT_STARTUP_RM})
    fi;
}

main() {
    flush

    readProgramme

    makePayload "${PROGRAMME_DIR_PATH}" "${PROGRAMME_DIR_NAME}" "${PROGRAMME_DIR_NEW_NAME}"

    readStartupScript

    makeStage "${STARTUP_SCRIPT}" "${UNPACK_PATH}"

    readMakeselfValue

    makeSelf "${MAKESELF_OUTPUT_NAME}" "${MAKESELF_OUTPUT_PATH}"
}

main "${@}"