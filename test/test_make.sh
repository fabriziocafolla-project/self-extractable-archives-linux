#!/bin/bash

. "$(pwd)/../functions/make.sh"

stub_err_msg() {
    case $1 in
    "missing_path")
        echo "Error: Missing project path"
    ;;
    "path_not_exist")
        echo "Error: Path is not a directory"
    ;;
    "empty_file")
        echo "Error: script file is empty"
    ;;
    esac
}

test_payload_success() {
    makePayload "$(pwd)/fake" "fake"

    assertNotNull ${TMP_DIR_PAYLOAD}

    if [[ ! -d "${TMP_DIR_PAYLOAD}" ]] ; then assertNull null ; fi

    if [[ ! -f "${TMP_DIR_PAYLOAD}/payload.tar" ]] ; then assertNull "File payload.tar exist in ${TMP_DIR_PAYLOAD} dir" null ; fi
}

test_payload_new_name_success() {
    makePayload "$(pwd)/fake" "fake" "test"

    if [[ ${PROGRAMME_DIR_NEW_NAME} != "" ]] ; then
        if [[ ! -d "${TMP_DIR_PAYLOAD}/${PROGRAMME_DIR_NEW_NAME}" ]] ; then assertNull "Directory ${PROGRAMME_DIR_NEW_NAME} exist into ${TMP_DIR_PAYLOAD} dir" null ; fi
    fi
}

test_payload_missing_path_fail(){
    local output=$(makePayload)

    assertEquals "$(stub_err_msg "missing_path")" "${output}"
}

test_payload_path_not_exist_fail(){
    local path=$(echo $(pwd)/fake_err)
    local output=$(makePayload "${path}")

    assertEquals "$(stub_err_msg "path_not_exist")" "${output}"
}

test_stage_success() {
    makeStage "$(pwd)/fake.sh" ""

    assertNotNull ${TMP_DIR_STAGE}

    if [[ ! -d "${TMP_DIR_STAGE}" ]] ; then assertNull null ; fi

    if [[ ! -f "${TMP_DIR_STAGE}/payload.tar" ]] ; then assertNull "File payload.tar exist in ${TMP_DIR_STAGE} dir" null ; fi

    if [[ ! -f "${TMP_DIR_STAGE}/install.sh" ]] ; then assertNull "File install.sh exist in ${TMP_DIR_STAGE} dir" null ; fi
}

test_stage_with_generate_script_success() {
   makeStage "@" "$(pwd)"

    assertNotNull ${TMP_DIR_STAGE}

    if [[ ! -d "${TMP_DIR_STAGE}" ]] ; then assertNull null ; fi

    if [[ ! -f "${TMP_DIR_STAGE}/payload.tar" ]] ; then assertNull "File payload.tar exist in ${TMP_DIR_STAGE} dir" null ; fi

    if [[ ! -f "${TMP_DIR_STAGE}/install.sh" ]] ; then assertNull "File install.sh exist in ${TMP_DIR_STAGE} dir" null ; fi
}

test_self_success(){
    makeSelf "test"

    if [[ ! -f "./test.run" ]] ; then assertNull "Not found installer in $(pwd)" null ; fi
}

test_self_different_path_success(){
    local path=$(echo "$(pwd)/fake")

    makeSelf "test" "${path}"

    if [[ ! -f "${path}/test.run" ]] ; then assertNull "Not found installer in ${path}" null ; fi
}

test_generate_script_success() {
    local output=$(generateStartupScript "$(pwd)")

    if [[ ! -f "${output}" ]] ; then assertNull "Generated file script not exist" null ; fi
}

test_generate_script_is_exe_success() {
    local output=$(generateStartupScript "$(pwd)")

    if [[ ! -x "${output}" ]] ; then assertNull "Generated file script is not executable" null ; fi
}

test_generate_script_missing_path_error() {
    local output=$(generateStartupScript)

    assertEquals "$(stub_err_msg "missing_path")" "${output}"
}

# load shunit2
. /usr/share/shunit2/shunit2