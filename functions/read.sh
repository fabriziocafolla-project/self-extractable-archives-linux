#!/usr/bin/env bash

#[INPUT] Variabile contenente il path del progetto da rendere selfmake
PROGRAMME_DIR_PATH=""
#[SYSTEM] Varibile contenente il nome del progetto (viene estratta dal path)
PROGRAMME_DIR_NAME=""
#[INPUT] Var contenente il nome da assegnare al progetto (in modo tale che nel tar abbia un nome diverso)
PROGRAMME_DIR_NEW_NAME=""

#[INPUT] File temporaneo contenente lo script di installazione necessario al makeself
STARTUP_SCRIPT=""
#[INPUT] Var contenente il path di dove dovrà essere estratto il programma (solo se si sceglie l'opzione @ in STARTUP_INPUT)
UNPACK_PATH=""

#[INPUT] Var contenente il nome del file selfmake generato
MAKESELF_OUTPUT_NAME=""
#[INPUT] Var contenente il path di dove il file selfmake deve essere salvato
MAKESELF_OUTPUT_PATH=""

readProgramme() {
    local count=0
    while : ; do
        read -p "1) Path of directory to make self-extractable (req.): " PROGRAMME_DIR_PATH

        [[ ${PROGRAMME_DIR_PATH} == "" ]] || break

        ((count++))

        [[ ${count} -lt 3 ]] || exit 0
    done

    PROGRAMME_DIR_NAME="$(echo ${PROGRAMME_DIR_PATH} | rev | cut -d/ -f1 | rev)"

    read -p "2) Rename folder when extract, press enter to skip: " PROGRAMME_DIR_NEW_NAME
}

readStartupScript() {
    read -p "3) Strat up script path or input '@' to auto-generate (req.): " STARTUP_SCRIPT

    if [[ ${STARTUP_SCRIPT} == "" ]] ; then
        echo "Error: missing script path"
        exit 0
    elif [[ ${STARTUP_SCRIPT} == "@" ]] ; then
        readUnpackDir
        return
    fi

    if [[ ! -f ${STARTUP_SCRIPT} ]] ; then
        echo "Error: missing script path / or failed generate it"
        exit 0
    fi
}

readMakeselfValue() {
    local count=0
    while : ; do
        read -p "4) Installer name (req.): " MAKESELF_OUTPUT_NAME

        read -p "5) Save installer in specific path, press enter to skip:" MAKESELF_OUTPUT_PATH

        [[ ${MAKESELF_OUTPUT_NAME} == "" ]] || break

        ((count++))

        [[ ${count} -lt 3 ]] || exit 0
    done
}

readUnpackDir(){
    read -p "3.a) Define destination path (req.): " UNPACK_PATH

    if [[ ${UNPACK_PATH} == "" ]] ; then
        echo "Error: missing unpack path"
        exit 0
    fi;
}