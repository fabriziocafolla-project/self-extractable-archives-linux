#!/usr/bin/env bash

#Cartella temporanea contenente la copia del progetto per poi creare un tar di essa
TMP_DIR_PAYLOAD=""
#Cartella temporanea che conterra il tar e il file di script (payload.tar e install.sh)
TMP_DIR_STAGE=""

#Richiede che siano eseguite le funzioni readProgramme
makePayload() {
    local _PROGRAMME_DIR_PATH=${1}
    local _PROGRAMME_DIR_NAME=${2:-}
    local _PROGRAMME_DIR_NEW_NAME=${3:-}

    if [[ ${_PROGRAMME_DIR_PATH} == "" ]] ; then
        echo "Error: Missing project path"
        exit 0;
    elif [[ ! -d ${_PROGRAMME_DIR_PATH} ]] ; then
        echo "Error: Path is not a directory"
        exit 0;
    fi;

    #create tmp dir
    TMP_DIR_PAYLOAD=$(mktemp -d "/tmp/payload_XXX_$(date +%F)")

    #Copy project into tmp dir
    cp -r "${_PROGRAMME_DIR_PATH}" "${TMP_DIR_PAYLOAD}"

    #Change project name if user input new name
    if [[ ${_PROGRAMME_DIR_NEW_NAME} != "" ]] ; then
        mv "${TMP_DIR_PAYLOAD}/${_PROGRAMME_DIR_NAME}" "${TMP_DIR_PAYLOAD}/${_PROGRAMME_DIR_NEW_NAME}"
    fi;

    #Create tar from payload dir
    tar -cvf "${TMP_DIR_PAYLOAD}/payload.tar" -C "${TMP_DIR_PAYLOAD}" .
}

#Richiede che siano eseguite le funzioni makePayload e readStartupScript
makeStage(){
    local _STARTUP_SCRIPT=${1:-}
    local _UNPACK_PATH=${2:-}

    #Create tmp dir
    TMP_DIR_STAGE=$(mktemp -d "/tmp/stage_XXX_$(date +%F)")

    #Copy tar in stage dir
    cp -r "${TMP_DIR_PAYLOAD}/payload.tar" "${TMP_DIR_STAGE}"

    if [[ ${_STARTUP_SCRIPT} == "@" ]] && [[ ${_UNPACK_PATH} != "" ]] ; then
        _STARTUP_SCRIPT=$(generateStartupScript "${_UNPACK_PATH}")
    fi

    #Copy startup script in stage dir
    cp "${_STARTUP_SCRIPT}" "${TMP_DIR_STAGE}/install.sh"
}

#Richiede che siano eseguite le funzioni makeStage e readMakeselfValue
makeSelf() {
    local _MAKESELF_OUTPUT_NAME=${1}
    local _MAKESELF_OUTPUT_PATH=${2}

    #PROGRAMME_SCRIPT_NAME="$(echo ${STARTUP_SCRIPT} | rev | cut -d/ -f1 | rev)"
    makeself "${TMP_DIR_STAGE}" "${_MAKESELF_OUTPUT_NAME}.run" "MY APP" "./install.sh"

    if [[ -d ${_MAKESELF_OUTPUT_PATH} ]] ; then
        mv "./${_MAKESELF_OUTPUT_NAME}.run" ${_MAKESELF_OUTPUT_PATH}
    fi;
}

#Genera un file sh temporaneo che conterrà lo script di startup dell'archivio (richiede un path come parametro che è quello dove dovranno essere estratti i file)
generateStartupScript(){
    local _UNPACK_PATH=${1:-}

    if [[ ${_UNPACK_PATH} == "" ]] ; then
        echo "Error: Missing project path"
        exit 0
    fi

    local TMP_SCRIPT_FILE=$(mktemp "/tmp/script_installer.XXX")

cat <<EOF >${TMP_SCRIPT_FILE}
#!/bin/bash

DIR="${_UNPACK_PATH:-}"

main() {
    if [[ ! -d \${DIR} ]] ; then
        mkdir -p \${DIR}
    fi;

    tar -xvf ./payload.tar -C "\${DIR}" &&
    echo "Config files extracted successfully!"
}

main "\${@}"
EOF

    chmod +x ${TMP_SCRIPT_FILE}

    echo "${TMP_SCRIPT_FILE}"
}